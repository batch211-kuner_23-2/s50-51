//import { useState, useEffect } from 'react';
import { Link } from 'react';
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}){

  // console.log(props)

  // useState hook:
  // a hook in React is a kind of tool. The useState hook allows creation and manipulation of states
  
  // States are a way for React to keep track of any value and associate it with a component

  // When a state changes,React re-renders only the specific component or part of the component that changed (and not the entire page or components who states have not changed)

  
  // state: a special kind of variable (can be named anything) that React uses to render/re-render components when needed

  // state setter: state setters are the only way to change a states value.

  // array destructuring to get the state and the setter 
  const [count, setCount] = useState(0)
  const [seats, setSeats] = useState(10)

  let { name, description, price } = courseProp;

  //refactor the enroll function and instead use "useEffect"
  // function enroll(){
  //   if(count!==10){
  //           setCount(count + 1);
  //           setSeats(seats - 1);
  //      }

  //   }
    // Apply the use effect hook
    // useEffect makes any given code block happen when a state changes and when a component first mounts (such as initial page load)

    // Syntax:
      // useEffect(function,[dependencies])
    
    // useEffect(()=>{
    //   if(seats===0){
    //       alert("No more seats available!")
    //   }
    // }, [count, seats])
  

//alert("No more seats available!")
  return(
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PHP {price}</Card.Text>
        <Card.Text>Enrollees: {count}</Card.Text>
        <Card.Text>Available Seats: {seats}</Card.Text>
        <Button as={"Link"} to={}>Enroll</Button>
      </Card.Body>
    </Card>
  )
}
