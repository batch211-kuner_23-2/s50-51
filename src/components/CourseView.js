import { useState } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams } from 'react-router-dom';

export default function CourseView() {

	const { courseId } = useParams()

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const[price, setPrice] = useState(0);

	useEffect(()=>{

	},[courseId])

	return(
	<Container className="mt-5">
		<Row>
			<Col lg={{span:6, offset:3}}>
			    <Card>
			      <Card.Body>
			        <Card.Title>{name}</Card.Title>
			        <Card.Subtitle>Description:</Card.Subtitle>
			        <Card.Text>{description}</Card.Text>
			        <Card.Subtitle>Price:</Card.Subtitle>
			        <Card.Text>PHP {price}</Card.Text>
			        <Card.Text>Class Schedule</Card.Text>
			        <Card.Text>8:00AM to 5:00PM</Card.Text>
			        <Button variant="primary" >Enroll</Button>
			      </Card.Body>
			    </Card>
			 </Col>
		 </Row>
	 </Container>   
  )
}